/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: main.cpp
        Version:   v0.0
        Date:      19-05-2020
    ============================================================================
 */

/*
** =============================================================================
**                        INCLUDE STATEMENTS
** =============================================================================
*/

#include "main.h"

#define DHTTYPE DHT11
#define DHTPIN 7
#define SensorPin A3

typedef enum {
  wdt_16ms = 0,
  wdt_32ms,
  wdt_64ms,
  wdt_128ms,
  wdt_250ms,
  wdt_500ms,
  wdt_1s,
  wdt_2s,
  wdt_4s,
  wdt_8s
} wdt_prescalar_e;

DHT dht(DHTPIN, DHTTYPE);

const byte LED = 2;
const byte LDO_EN = 10;

const int csPin = 6;    // LoRa radio chip select
const int resetPin = 5; // LoRa radio reset
const int irqPin = 3; // change for your board; must be a hardware interrupt pin

int counter = 0;

float h = 0;
float t = 0;

float soilValue = 0;

const short sleep_cycles_per_transmission = 60;
volatile short sleep_cycles_remaining = sleep_cycles_per_transmission;

// watchdog interrupt
ISR(WDT_vect) { --sleep_cycles_remaining; } // end of WDT_vect


/*
** =============================================================================
**                   LOCAL  FUNCTION DECLARATIONS
** =============================================================================
*/
void put_to_sleep(void);
void get_dht11_data(void);
void lora_send(void);
void power_enable(void);
void power_disable(void);
void work(void);
int getBandgap(void);
void get_soil_sens(void);
void setup_watchdog(uint8_t prescalar);


/*==============================================================================
** Function...: setup
** Return.....: void
** Description: Main function setup
** Created....: 19.05.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void setup() {

  // Setup watchdog to interrupt every 8 sec.
  setup_watchdog(wdt_8s);

  pinMode(LDO_EN, OUTPUT);
  digitalWrite(LDO_EN, HIGH);
}

/*==============================================================================
** Function...: loop
** Return.....: void
** Description: Main function loop
** Created....: 19.05.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void loop() {

  work();

  while (sleep_cycles_remaining) {
    put_to_sleep();
  }

  sleep_cycles_remaining = sleep_cycles_per_transmission;
}

/*==============================================================================
** Function...: setup
** Return.....: void
** Description: watchdog  setup.
**              Arguments:
**              0=16ms, 1=32ms,2=64ms,3=125ms,4=250ms,5=500ms
**              6=1 sec,7=2 sec, 8=4 sec, 9= 8sec
** Created....: 28.11.2014 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void setup_watchdog(uint8_t prescalar) {
  prescalar = min(9, prescalar);
  uint8_t wdtcsr = prescalar & 7;
  if (prescalar & 8)
    wdtcsr |= _BV(WDP3);

  MCUSR &= ~_BV(WDRF);
  WDTCSR = _BV(WDCE) | _BV(WDE);
  WDTCSR = _BV(WDCE) | wdtcsr | _BV(WDIE);
}

void work(void) {
  delay(2000);
  get_soil_sens();
  get_dht11_data();
  lora_send();
}

void lora_send(void) {

  LoRa.setPins(csPin, resetPin, irqPin);
  LoRa.begin(868E6);

  LoRa.beginPacket();
  LoRa.print("#0, ");
  LoRa.print(t);
  LoRa.print(", ");
  LoRa.print(soilValue);
  LoRa.print(", ");
  LoRa.print(getBandgap());
  LoRa.endPacket();
  LoRa.end();
}

void get_soil_sens(void) {
  for (int i = 0; i <= 100; i++) {
    soilValue = soilValue + analogRead(SensorPin);
    delay(1);
  }
  soilValue = soilValue / 100.0;
}

void get_dht11_data(void) {

  dht.begin();

  t = dht.readTemperature();

}

void put_to_sleep(void) {

  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here
  sleep_enable();

  // turn off brown-out enable in software
  MCUCR = bit(BODS) | bit(BODSE);
  MCUCR = bit(BODS);

  byte keep_ADCSRA = ADCSRA;
  // disable ADC
  ADCSRA = 0;

  byte keep_PRR = PRR;
  // turn off various modules
  PRR = 0xFF;

  sleep_cpu(); // System sleeps here

  sleep_disable(); // System continues execution here when watchdog timed out

  PRR = keep_PRR;
  // enable ADC
  ADCSRA = keep_ADCSRA;
}

int getBandgap(void) // Returns actual value of Vcc (x 100)
{

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
  // For mega boards
  const long InternalReferenceVoltage =
      1115L; // Adjust this value to your boards specific internal BG voltage
             // x1000
  // REFS1 REFS0          --> 0 1, AVcc internal ref. -Selects AVcc reference
  // MUX4 MUX3 MUX2 MUX1 MUX0  --> 11110 1.1V (VBG)         -Selects channel 30,
  // bandgap voltage, to measure
  ADMUX = (0 << REFS1) | (1 << REFS0) | (0 << ADLAR) | (0 << MUX5) |
          (1 << MUX4) | (1 << MUX3) | (1 << MUX2) | (1 << MUX1) | (0 << MUX0);

#else
  // For 168/328 boards
  const long InternalReferenceVoltage =
      1056L; // Adjust this value to your boards specific internal BG voltage
             // x1000
  // REFS1 REFS0          --> 0 1, AVcc internal ref. -Selects AVcc external
  // reference MUX3 MUX2 MUX1 MUX0  --> 1110 1.1V (VBG)         -Selects channel
  // 14, bandgap voltage, to measure
  ADMUX = (0 << REFS1) | (1 << REFS0) | (0 << ADLAR) | (1 << MUX3) |
          (1 << MUX2) | (1 << MUX1) | (0 << MUX0);

#endif
  delay(50); // Let mux settle a little to get a more stable A/D conversion
  // Start a conversion
  ADCSRA |= _BV(ADSC);
  // Wait for it to complete
  while (((ADCSRA & (1 << ADSC)) != 0))
    ;
  // Scale the value
  int results = (((InternalReferenceVoltage * 1024L) / ADC) + 5L) /
                10L; // calculates for straight line value
  return results;
}