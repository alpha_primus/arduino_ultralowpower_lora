/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: main.cpp
        Version:   v0.0
        Date:      19-05-2020
    ============================================================================
 */

/*
** =============================================================================
**                        INCLUDE STATEMENTS
** =============================================================================
*/

#ifndef MAIN_H
#define	MAIN_H

#include <Arduino.h>
#include <DHT.h>
#include <LoRa.h>
#include <SPI.h>
#include <Wire.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

#endif	/* MAIN_H */